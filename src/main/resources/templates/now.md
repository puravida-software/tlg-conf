Próximas sesiones:

<%sessions.each{ session->
    println "\t_${session.title}_ (${session.whenTime})"
    println "\t$session.description"
    if( session.url )
        println "\t[Saber mas]($session.url)"
    println "-".multiply(20)
}%>