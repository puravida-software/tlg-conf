<%conference.days.each{ day->
    println "__*${day.whenDate}*__"
    println ""    
    day.tracks.each{ track ->
        println "Track *${track.code}*"
        track.sessions.each{ session->
            println "\t_${session.title}_ (${session.whenTime})"
            println "\t$session.description"
            if( session.url )
                println "\t[Saber mas]($session.url)"
            println ""
        }
        println "-".multiply(20)
    }
}%>