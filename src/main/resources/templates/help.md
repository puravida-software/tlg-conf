__*Help*__

You can send me these __*commands*__

/agenda
(navigate across the full agenda)

/random
(can't decide, choose next conference for me)

/tags
(search by topics)
