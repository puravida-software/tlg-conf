package com.puravida.mn.telegram;
import io.micronaut.core.annotation.Introspected;

@Introspected
public class Message {
    int message_id;
    String chat_id;
    String text;
    String parse_mode = "markdown";
    Location location;
    ReplyMarkup reply_markup;

    public int getMessage_id(){
        return message_id;
    }

    public void setMessage_id(int message_id){
        this.message_id=message_id;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getParse_mode() {
        return parse_mode;
    }

    public void setParse_mode(String parse_mode) {
        this.parse_mode = parse_mode;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ReplyMarkup getReply_markup() {
        return reply_markup;
    }

    public void setReply_markup(ReplyMarkup reply_markup) {
        this.reply_markup = reply_markup;
    }
}
