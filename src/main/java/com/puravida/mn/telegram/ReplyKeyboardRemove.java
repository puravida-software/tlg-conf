package com.puravida.mn.telegram;
import io.micronaut.core.annotation.Introspected;

@Introspected
public class ReplyKeyboardRemove extends ReplyMarkup{

    boolean remove_keyboard = true;

    public boolean isRemove_keyboard() {
        return remove_keyboard;
    }

    public void setRemove_keyboard(boolean remove_keyboard) {
        this.remove_keyboard = remove_keyboard;
    }
}
