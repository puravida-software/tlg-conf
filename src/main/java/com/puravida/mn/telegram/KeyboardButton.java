package com.puravida.mn.telegram;
import io.micronaut.core.annotation.Introspected;

@Introspected
public class KeyboardButton {
    String text;
    boolean request_contact = false;
    boolean request_location = false;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isRequest_contact() {
        return request_contact;
    }

    public void setRequest_contact(boolean request_contact) {
        this.request_contact = request_contact;
    }

    public boolean isRequest_location() {
        return request_location;
    }

    public void setRequest_location(boolean request_location) {
        this.request_location = request_location;
    }
}
