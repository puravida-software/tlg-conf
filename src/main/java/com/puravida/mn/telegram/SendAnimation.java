package com.puravida.mn.telegram;

import io.micronaut.http.MediaType;
import io.micronaut.http.client.multipart.MultipartBody;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SendAnimation extends SendMedia{

    @Override
    protected String getMedia() {
        return "animation";
    }

    @Override
    protected String getSuffix() {
        return "gif";
    }
}