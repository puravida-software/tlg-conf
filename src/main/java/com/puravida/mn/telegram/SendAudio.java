package com.puravida.mn.telegram;

public class SendAudio extends SendMedia{
    @Override
    protected String getMedia() {
        return "audio";
    }

    @Override
    protected String getSuffix() {
        return "mp3";
    }
}
