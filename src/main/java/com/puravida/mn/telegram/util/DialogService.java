package com.puravida.mn.telegram.util;

import com.puravida.mn.telegram.Update;

public interface DialogService {

    void answer( Update update);

    void answerCallback( Update udpate);

}
