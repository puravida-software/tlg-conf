package com.puravida.mn.telegram;

public class SendPhoto extends SendMedia{

    @Override
    protected String getMedia() {
        return "photo";
    }

    @Override
    protected String getSuffix() {
        return "png";
    }
}
