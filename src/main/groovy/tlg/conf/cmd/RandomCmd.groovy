package tlg.conf.cmd

import com.puravida.mn.telegram.Message
import com.puravida.mn.telegram.TelegramBot
import com.puravida.mn.telegram.Update
import tlg.conf.TemplateService
import tlg.conf.model.Conference
import tlg.conf.model.Session

class RandomCmd {

    Conference conference

    TemplateService templateService

    void start(Update update, TelegramBot telegramBot){

        Date now = new Date()
        List<Session> list = conference.sessions.find{
            it.value.find{ it.ini > now}
        }?.value ?: conference.sessions.entrySet().sort{ it.key }.first().value

        Session selected = list[ new Random().nextInt(list.size()) ]
        String text = templateService.session(selected)
        String chatId = update.message.chat.id
        telegramBot.sendMessage( new Message(
                chat_id: chatId,
                text: text
        )).subscribe({ println "done random"}, { err -> println err.message})
    }

}
