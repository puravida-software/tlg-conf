package tlg.conf.cmd
import com.puravida.mn.telegram.InlineKeyboardButton
import com.puravida.mn.telegram.InlineKeyboardMarkup
import com.puravida.mn.telegram.Message
import com.puravida.mn.telegram.ReplyMarkup
import com.puravida.mn.telegram.TelegramBot
import com.puravida.mn.telegram.Update
import tlg.conf.TemplateService
import tlg.conf.model.Conference
import tlg.conf.model.Session

class TagsCmd {

    Conference conference

    TemplateService templateService

    void setConference(Conference conference){
        this.conference = conference
    }

    void start(Update update, TelegramBot telegramBot){
        String chatId = update.message.chat.id
        telegramBot.sendMessage( new Message(
                chat_id: chatId,
                text: "Tags",
                reply_markup: startKeyboard()
        )).subscribe({ println "done start"}, { err -> println err.message})
    }

    void answer(Update update, TelegramBot telegramBot) {
        String chatId = update.callback_query.message.chat.id
        String[] cmd = update.callback_query.data.split(' ').drop(1)
        switch( cmd[0] ){
            case 'cancel':
                deleteMessage(update, telegramBot)
                break
            case 'home':
                replaceKeyboard(update, startKeyboard(), telegramBot)
                break
            case 'tag':
                replaceKeyboard(update, tagKeyboard(cmd[1]), telegramBot)
                break
            case 'show':
                showSession(update, cmd[1], telegramBot)
                break
        }
    }

    void deleteMessage(Update update, TelegramBot telegramBot){
        String chatId = update.callback_query.message.chat.id
        int messageId = (update.callback_query?.inline_message_id ?: update.callback_query.message.message_id)
        telegramBot.deleteMessage( chatId, messageId).subscribe()
    }

    InlineKeyboardMarkup startKeyboard(){

        List<List<InlineKeyboardButton>> keyboard = []

        conference.tags.eachWithIndex { entry , idx ->
            keyboard.add([new InlineKeyboardButton(
                    text: "${idx+1} .- $entry.key",
                    callback_data: "tags tag $entry.key"
            )])
        }

        List<InlineKeyboardButton> buttons = []

        buttons.add new InlineKeyboardButton(
                text: "Close",
                callback_data: "tags cancel"
        )

        keyboard.add buttons
        println "build keyboard ${new Date()}"
        new InlineKeyboardMarkup(inline_keyboard: keyboard)
    }

    InlineKeyboardMarkup tagKeyboard( String tag){
        List<List<InlineKeyboardButton>> keyboard = []
        conference.tags.find{ it.key == tag}.value.eachWithIndex{ Session session , int idx ->
            keyboard.add([new InlineKeyboardButton(
                    text: "${idx+1} .- $session.title",
                    callback_data: "tags show $session.uuid"
            )])
        }
        List<InlineKeyboardButton> buttons = []

        buttons.add new InlineKeyboardButton(
                text: "<<",
                callback_data: "tags home"
        )

        keyboard.add buttons

        new InlineKeyboardMarkup(inline_keyboard: keyboard)
    }

    void replaceKeyboard(Update update, ReplyMarkup replyMarkup, TelegramBot telegramBot){
        String chatId = update.callback_query.message.chat.id
        int messageId = (update.callback_query?.inline_message_id ?: update.callback_query.message.message_id)
        telegramBot.editMessageReplyMarkup( chatId, messageId, replyMarkup ).subscribe()
    }

    void showSession(Update update, String uuid, TelegramBot telegramBot){
        Session show = conference.sessions.values().flatten().find{ it.uuid == uuid }
        if( !show )
            return

        String chatId = update.callback_query.message.chat.id

        String msg = templateService.session(show)

        msg = """
${show.title} (${show.track.code} ${show.whenTime})
${show.speakers ?: ''}
${show.description ?: ''}
"""
        telegramBot.answerCallbackQuery(update.callback_query.id, msg, true).subscribe()
    }
}
