package tlg.conf.cmd

import com.puravida.mn.telegram.Message
import com.puravida.mn.telegram.TelegramBot
import com.puravida.mn.telegram.Update
import tlg.conf.TemplateService
import tlg.conf.model.Conference


class LocationCmd {

    Conference conference

    void answer(Update update, TelegramBot telegramBot){
        String chatId = update.message.chat.id
        if( conference.latitude && conference.longitude)
            telegramBot.sendLocation(chatId, conference.latitude, conference.longitude).subscribe()
        else
            telegramBot.sendMessage(
                    new Message(chat_id: chatId, text: "Ojala supiera donde es, pero no me lo han dicho")).subscribe()
    }

}
