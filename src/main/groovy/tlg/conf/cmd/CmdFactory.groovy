package tlg.conf.cmd

import com.puravida.mn.telegram.TelegramBot
import tlg.conf.ConferenceProvider
import tlg.conf.TemplateService

import jakarta.inject.Singleton

@Singleton
class CmdFactory {

    TemplateService templateService

    ConferenceProvider conferenceProvider

    CmdFactory(TemplateService templateService, ConferenceProvider conferenceProvider) {
        this.templateService = templateService
        this.conferenceProvider = conferenceProvider
    }

    WelcomeCmd newWelcomeCmd(){
        new WelcomeCmd(templateService: templateService, conference: conferenceProvider.conference)
    }

    EndCmd newEndCmd(){
        new EndCmd(templateService: templateService, conference: conferenceProvider.conference )
    }

    DynAgendaCmd newDynAgendaCmd(){
        new DynAgendaCmd(templateService: templateService, conference: conferenceProvider.conference )
    }

    LocationCmd newLocationCmd(){
        new LocationCmd(templateService: templateService, conference: conferenceProvider.conference )
    }

    RandomCmd newRandomCmd(){
        new RandomCmd(templateService: templateService, conference: conferenceProvider.conference)
    }

    TagsCmd newTagsCmd(){
        new TagsCmd(templateService: templateService, conference: conferenceProvider.conference)
    }

    SessionCmd newSessionCmd(){
        new SessionCmd(templateService: templateService, conference: conferenceProvider.conference)
    }

    HelpCmd newHelpCmd(){
        new HelpCmd(templateService: templateService, conference: conferenceProvider.conference)
    }
}
