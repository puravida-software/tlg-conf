package tlg.conf.cmd

import com.puravida.mn.telegram.InlineKeyboardButton
import com.puravida.mn.telegram.InlineKeyboardMarkup
import com.puravida.mn.telegram.Message
import com.puravida.mn.telegram.ReplyMarkup
import com.puravida.mn.telegram.TelegramBot
import com.puravida.mn.telegram.Update
import tlg.conf.TemplateService
import tlg.conf.model.Conference
import tlg.conf.model.Session

class DynAgendaCmd {

    Conference conference

    TemplateService templateService

    void start(Update update, TelegramBot telegramBot){
        String chatId = update.message.chat.id
        telegramBot.sendMessage( new Message(
                chat_id: chatId,
                text: "Agenda",
                reply_markup: startKeyboard()
        )).subscribe({ println "done start"}, { err -> println err.message})
    }

    void answer(Update update, TelegramBot telegramBot){
        String chatId = update.callback_query.message.chat.id
        String[] cmd = update.callback_query.data.split(' ').drop(1)
        switch( cmd[0] ){
            case 'cancel':
                deleteMessage(update, telegramBot)
                break
            case 'home':
                replaceKeyboard(update, startKeyboard(), telegramBot)
                break
            case 'prev':
                replaceKeyboard(update, prevKeyboard(cmd[1]), telegramBot)
                break
            case 'next':
                replaceKeyboard(update, nextKeyboard(cmd[1]), telegramBot)
                break
            case 'show':
                showSession(update, cmd[1], telegramBot)
                break
        }
    }

    void deleteMessage(Update update, TelegramBot telegramBot){
        String chatId = update.callback_query.message.chat.id
        int messageId = (update.callback_query?.inline_message_id ?: update.callback_query.message.message_id)
        telegramBot.deleteMessage( chatId, messageId).subscribe({},{t->println "error borrando mensaje $t"})
    }

    void replaceKeyboard(Update update, ReplyMarkup replyMarkup, TelegramBot telegramBot){
        String chatId = update.callback_query.message.chat.id
        int messageId = (update.callback_query?.inline_message_id ?: update.callback_query.message.message_id)
        telegramBot.editMessageReplyMarkup( chatId, messageId, replyMarkup ).subscribe({},{t->println "error reemplanzado keyboard $t"})
    }

    InlineKeyboardMarkup startKeyboard(){
        Date now = new Date()
        List<Session> list = conference.sessions.find{
            it.value.find{ it.ini > now}
        }?.value
        if( !list ){
            list = conference.sessions.entrySet().sort{ it.key }.first().value
        }
        buildKeyboard(list)
    }

    InlineKeyboardMarkup nextKeyboard(String find){
        List<Session> list
        Session founded
        for( Map.Entry<Date, List<Session>> entry : conference.sessions){
            if( founded ){
                list = entry.value
                break
            }
            founded = entry.value.find{ it.uuid == find }
        }
        if( ! list )
            list = conference.sessions.values().first()
        buildKeyboard(list)
    }

    InlineKeyboardMarkup prevKeyboard(String find){
        List<Session> list
        for( Map.Entry<Date, List<Session>> entry : conference.sessions){
            Session founded = entry.value.find{ it.uuid == find }
            if( founded )
                break
            list = entry.value
        }
        if( ! list )
            list = conference.sessions.values().last()
        buildKeyboard(list)
    }


    InlineKeyboardMarkup buildKeyboard(List<Session> show){

        List<List<InlineKeyboardButton>> keyboard = []

        show.eachWithIndex { Session s, int idx->
            if( !idx ){
                keyboard.add([new InlineKeyboardButton(
                        text: "${s.whenDate}, ${s.whenTime}",
                        callback_data: "none"
                )])
            }
            keyboard.add([new InlineKeyboardButton(
                    text: "$s.title",
                    callback_data: "schedule show $s.uuid"
            )])
        }

        List<InlineKeyboardButton> buttons = []

        if( show.size() ) {
            buttons.add new InlineKeyboardButton(
                    text: "<<",
                    callback_data: "schedule prev ${show.first().uuid}"
            )
        }

        buttons.add new InlineKeyboardButton(
                text: "Close",
                callback_data: "schedule cancel"
        )

        if( show.size() ) {
            buttons.add new InlineKeyboardButton(
                    text: ">>",
                    callback_data: "schedule next ${show.last().uuid}"
            )
        }

	    keyboard.add buttons	    
        new InlineKeyboardMarkup(inline_keyboard: keyboard)
    }


    void showSession(Update update, String uuid, TelegramBot telegramBot){
        Session show = conference.sessions.values().flatten().find{ it.uuid == uuid }
        if( !show )
            return

        final String chatId = update.callback_query.message.chat.id

        String msg = templateService.session(show)

        msg = """
*${show.title}*
(${show.track.code} ${show.whenTime})
${show.speakers ?: ''}
${show.url ?: ''}
""".take(200)
        
        telegramBot.answerCallbackQuery(update.callback_query.id, "", true).subscribe({
            println "showSession $uuid"
        }, {t->
            println "error mostrando sesion $t"
        })

        telegramBot.sendMessage( new Message(
            chat_id: chatId,
            text: msg,
        )).subscribe({ message -> 
            
            sleep 20*1000
            telegramBot.deleteMessage( chatId, message.result.message_id).subscribe({},{t->println "error borrando mensaje $t"})

        }, { err -> println err.message})
        
    }
}
