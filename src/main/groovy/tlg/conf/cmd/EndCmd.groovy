package tlg.conf.cmd


import com.puravida.mn.telegram.Message
import com.puravida.mn.telegram.TelegramBot
import com.puravida.mn.telegram.Update
import tlg.conf.TemplateService
import tlg.conf.model.Conference

class EndCmd {

    TemplateService templateService

    Conference conference

    void answer(Update update, TelegramBot telegramBot){
        String chatId = update.message.chat.id
        telegramBot.sendMessage( new Message( chat_id: chatId, text: """
Este evento ha terminado.

This event is already finished
""") ).subscribe()
    }
}
