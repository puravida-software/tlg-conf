package tlg.conf.cmd

import com.puravida.mn.telegram.InlineKeyboardButton
import com.puravida.mn.telegram.InlineKeyboardMarkup
import com.puravida.mn.telegram.Message
import com.puravida.mn.telegram.ReplyMarkup
import com.puravida.mn.telegram.TelegramBot
import com.puravida.mn.telegram.Update
import tlg.conf.TemplateService
import tlg.conf.model.Conference
import tlg.conf.model.Session

class SessionCmd {

    Conference conference

    TemplateService templateService

    void answer(Update update, String uuid, TelegramBot telegramBot){
        Session show = conference.sessions.values().flatten().find{ it.uuid == uuid }
        if( !show )
            return

        String text = templateService.session(show)
        String chatId = update.callback_query.message.chat.id
        telegramBot.sendMessage( new Message(
                chat_id: chatId,
                text: text
        )).subscribe({ println "done show"}, { err -> println err.message})
    }

}
