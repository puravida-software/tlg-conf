package tlg.conf.model

class Track {

    String code
    Session[] sessions

    Day day

    void setDay(Day day){
        this.day = day
        sessions*.track = this
    }
}
