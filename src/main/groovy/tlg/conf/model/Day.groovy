package tlg.conf.model

class Day implements DateIniDateEnd{

    List<Track> tracks

    Conference conference

    void init(Conference conference, int day){
        this.conference = conference

        Calendar c = Calendar.instance
        c.time = conference.ini
        c.add Calendar.DAY_OF_MONTH, day

        ini = c.time

        tracks*.day = this
    }
}
