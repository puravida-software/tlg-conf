package tlg.conf.model

class Conference implements DateIniDateEnd{

    String name
    String url
    float latitude
    float longitude

    List<Day> days = []
    Map<Date, List<Session>> sessions = [:]

    List< Map.Entry<String, List<Session>>> tags = []

    void init(){
        println "init conference $name"
        this.with { Conference conference ->
            conference.days.eachWithIndex { Day entry, int i ->
                entry.init(conference, i)
            }

            conference.sessions = conference.days*.tracks*.sessions.flatten().sort { Session a, Session b ->
                (a.ini <=> b.ini) ?: a.title <=> b.title
            }.groupBy { Session a ->
                a.ini
            }.sort { it.key }

            Map< String, List<Session>> tmp = [:]
            conference.days*.tracks*.sessions.flatten().findAll{ it.tags }. each{ Session s ->
                String[] list = s.tags.split(',')
                list.each{ t->
                    t = t.trim()
                    def l = tmp[t] ?: []
                    l.add(s)
                    tmp[t] = l
                }
            }
            tags = tmp.entrySet().sort{ a->
                -1*a.value.size()
            }
        }
    }

}
