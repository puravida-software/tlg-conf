package tlg.conf.model

import java.text.SimpleDateFormat

class Session implements TimeIniTimeEnd{

    String uuid
    String title
    String speakers
    String description
    String url
    String tags

    Track track

    static int index

    void setTrack(Track track){
        this.track = track
        this.uuid = "${index++}"
        init(track.day.ini)
    }

    @Override
    String toString() {
        "$title $whenDate $whenTime"
    }
}
