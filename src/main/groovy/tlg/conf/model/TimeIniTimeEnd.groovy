package tlg.conf.model

import java.text.SimpleDateFormat

trait TimeIniTimeEnd {

    Date ini
    Date end

    int from
    int to

    void init(Date date){
        Calendar c

        c = Calendar.instance
        c.time = date
        c.set Calendar.MINUTE, from
        ini = c.time

        c = Calendar.instance
        c.time = date
        c.set Calendar.MINUTE, to
        end = c.time
    }

    String getIniDate(){
        new SimpleDateFormat("dd/MM").format(ini)
    }
    String getEndDate(){
        new SimpleDateFormat("HH:mm").format(end)
    }

    String getIniTime(){
        new SimpleDateFormat("HH:mm").format(ini)
    }
    String getEndTime(){
        new SimpleDateFormat("HH:mm").format(end)
    }

    String getWhenDate(){
        return iniDate
    }
    String getWhenTime(){
        if(from && to)
            return "$iniTime - $endTime"
        if( from )
            return iniTime
        return endTime
    }

}
