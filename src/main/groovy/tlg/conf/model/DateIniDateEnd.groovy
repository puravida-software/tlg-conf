package tlg.conf.model

import java.text.SimpleDateFormat

trait DateIniDateEnd {

    Date ini
    Date end

    String getIniDate(){
        new SimpleDateFormat("dd/MM").format(ini)
    }
    String getEndDate(){
        new SimpleDateFormat("dd/MM").format(end)
    }

    String getWhenDate(){
        if( ini && end )
            return "$iniDate - $endDate"
        if( ini )
            return iniDate
        return endDate
    }

}