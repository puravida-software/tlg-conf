package tlg.conf

import com.puravida.mn.telegram.TelegramBot
import com.puravida.mn.telegram.Update
import tlg.conf.cmd.CmdFactory
import tlg.conf.model.Conference

import jakarta.inject.Singleton

@Singleton
class DialogService {

    CmdFactory cmdFactory

    TelegramBot telegramBot

    DialogService(CmdFactory cmdFactory, TelegramBot telegramBot) {
        this.cmdFactory = cmdFactory
        this.telegramBot = telegramBot
    }

    void answer(Update update ) {
        switch (update.message.text) {
            case '/start':
            case '/welcome':
                cmdFactory.newWelcomeCmd().answer(update, telegramBot)
                break
            case '/agenda':
                if( cmdFactory.conferenceProvider.conference.ini < new Date() &&
                        new Date() > cmdFactory.conferenceProvider.conference.end){
                    cmdFactory.newEndCmd().answer(update, telegramBot)
                }else {
                    cmdFactory.newDynAgendaCmd().start(update, telegramBot)
                }
                break
            case '/random':
                cmdFactory.newRandomCmd().start(update,telegramBot)
                break
            case '/tags':
                cmdFactory.newTagsCmd().start(update,telegramBot)
                break
            case '/help':
                cmdFactory.newHelpCmd().start(update,telegramBot)
                break
        }

        if( update.message.location ){
            cmdFactory.newLocationCmd().answer(update, telegramBot)
        }
    }

    void answerCallback(Update update ){
        switch (update?.callback_query.data){
            case { "$it".startsWith('schedule')}:
                cmdFactory.newDynAgendaCmd().answer(update, telegramBot)
                break
            case { "$it".startsWith('tags')}:
                cmdFactory.newTagsCmd().answer(update, telegramBot)
                break
            case { "$it".startsWith('show')}:
                cmdFactory.newSessionCmd().answer(update, update?.callback_query.data.split(' ')[1], telegramBot)
                break

        }
    }

}
