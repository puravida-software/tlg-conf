package tlg.conf

import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import com.puravida.mn.telegram.Update
import reactor.core.publisher.Mono

@Controller('/${telegram.token}')
class BotController {

    DialogService dialogService
    ConferenceProvider conferenceProvider

    BotController(DialogService dialogService, ConferenceProvider conferenceProvider) {
        this.dialogService = dialogService
        this.conferenceProvider = conferenceProvider
    }

    @Get("/")
    String ping(){
        "ok"
    }

    @Get("/reload")
    String reload(){
        conferenceProvider.init()
        "ok"
    }

    @Post("/")
    Boolean onMessage(@Body Update update){        
        Mono.create{ emitter ->
            if( update.message )
                dialogService.answer(update)
            else
                dialogService.answerCallback(update)
            emitter.success(true)
        }.subscribe()
        true
    }


    @Get('/shutdown')
    String onShutdown(){
        Mono.create{ emitter ->
            conferenceProvider.init()
            emitter.success(true)
        }.subscribe()
        "amos alla"
    }

}
