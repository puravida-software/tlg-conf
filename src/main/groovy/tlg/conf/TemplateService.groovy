package tlg.conf

import groovy.text.SimpleTemplateEngine
import io.micronaut.context.annotation.Value
import io.micronaut.core.io.ResourceResolver
import tlg.conf.model.Session

import javax.annotation.PostConstruct
import jakarta.inject.Singleton

@Singleton
class TemplateService {

    ResourceResolver resourceLoader

    SimpleTemplateEngine engine = new SimpleTemplateEngine()

    TemplateService(ResourceResolver resourceLoader){
        this.resourceLoader = resourceLoader
    }

    @Value('${template.welcome:`classpath:templates/welcome.md`}')
    String welcomePath

    String welcome

    private void initWelcome(){
        if( welcomePath.startsWith("http") ){
            welcome = welcomePath.toURL().getText('UTF-8')
        }else {
            welcome = new InputStreamReader(
                    resourceLoader.getResource(welcomePath).get().openStream(), 'UTF-8'
            ).text
        }
        welcome += "\n\nMade with ❤️ by Puravida Software"
    }

    @Value('${template.session:`classpath:templates/session.md`}')
    String sessionPath

    private String sessionTxt

    private void initSession(){
        if( sessionPath.startsWith('http')){
            sessionTxt= sessionPath.toURL().getText('UTF-8')
        }else {
            sessionTxt = new InputStreamReader(
                    resourceLoader.getResource(sessionPath).get().openStream(), 'UTF-8'
            ).text
        }
    }

    @Value('${template.help:`classpath:templates/help.md`}')
    String helpPath

    String help

    private void initHelp(){
        if( helpPath.startsWith("http") ){
            help = helpPath.toURL().getText('UTF-8')
        }else {
            help = new InputStreamReader(
                    resourceLoader.getResource(helpPath).get().openStream(), 'UTF-8'
            ).text
        }
        help += "\n\nMade with ❤️ by Puravida Software"
    }

    String session( Session session){
        engine.createTemplate(sessionTxt).make([session:session])
    }

    @PostConstruct
    void init(){
        initWelcome()
        initSession()
        initHelp()
    }
}
