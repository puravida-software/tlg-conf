package tlg.conf

import io.micronaut.core.io.ResourceResolver
import org.yaml.snakeyaml.Yaml
import tlg.conf.model.Conference

import javax.annotation.PostConstruct
import jakarta.inject.Singleton
import tlg.conf.model.Session

@Singleton
class ConferenceProvider {

    ResourceResolver resourceLoader

    ConferenceProvider(ResourceResolver resourceLoader){
        this.resourceLoader = resourceLoader
    }

    Conference getConference(){
        if( !theConference ){
            init();
        }
        theConference
    }

    private Conference theConference

    @PostConstruct
    void init(){
        try {
            String path = System.getenv("REMOTE_AGENDA") ?: "classpath:templates/agenda.yml"
            String txt
            if (path.startsWith('http')) {
                txt = path.toURL().getText('UTF-8')
            } else {
                txt = new InputStreamReader(
                        resourceLoader.getResource(path).get().openStream(), 'UTF-8'
                ).text
            }

            theConference = new Yaml().loadAs(txt, Conference)
            Session.index=0
            theConference.init()
            println theConference
        }catch(Exception e){
            e.printStackTrace()
        }
    }

}
