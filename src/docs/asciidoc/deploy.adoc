
== Deploy

You will need to tell to Telegram where is located your boot via a public https URL
(i.e. https://my-organization.org/tlg-conf/bot/) so you need to deploy it in a server accessible
by Internet.

You have, at least, these options:

- Deploy in a server with java and an Internet connection (tested)
- Dockerize and deploy in a serverless cloud provider, Google Run for example (not tested)
- Dockerize and deploy in a Kubernetes cluster cloud provider as provide by Okteto (tested)

In any case the application will search an environment variable called *TELEGRAM_TOKEN* you must to provide
with the token value obtained from the `BotFather`

