
== Build

=== Requirements

This project requires Java 8 (min) and you can build it executing the following command:

[source,console]
----
./gradlew build
----

if everything is ok you'll have a runnable jar at `build/libs/tlg-conf-VERSION-all.jar` that you need
to deploy in a server accessible via Internet (see deploy section)

In case you want to dockerize the bot, this project also provides a `Dockerfile` file:

[source,console]
----
docker build -t jagedn/tlg-conf .
----

Also we have a `k8s.yml` file as *template* to deploy it in a Kubernetes cluster.

[source,console]
----
kubectl apply -f k8s.yml
----
