
== Welcome

The welcome message is a short Markdown file you can provide
with the information you want. It will be sent to the user at the start of the chat or when
the user request it via `/start` command

WARNING: Telegram support only a *subset* of Markdown. Bold, italic, and so on, see Telegram
documentation to check what you can use

Welcome can be a good place, for example, to promote your sponsors including link to their
websites and so on.

This is an exampleo of a `welcome.md`

[source]
----
__*Welcome to TestConference*__
The most awesonic conference about Tech

__*When:*__ 23-26 April 2020

__*Where:*__ Here, in *Madrid*

This event can't happend with the support of:

[Company 1](https://example.org) as Gold sponsor
[Company 2](https://example.org) as Platinum sponsor

Buy your ticket at https://example.org
----

More information at https://core.telegram.org/bots/api#markdownv2-style


