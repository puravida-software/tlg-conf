__*Welcome to JBCNConf 2022*__
The most awesonic conference about Tech

__*When:*__ 23-26 April 2022

__*Where:*__ Here, in *Barcelona*

You can send me these __*commands*__

/agenda
(view the full agenda)

/random
(can't decide, choose for me)

/tags
(search by topics)
